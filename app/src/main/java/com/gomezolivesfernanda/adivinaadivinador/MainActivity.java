package com.gomezolivesfernanda.adivinaadivinador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;
import java.util.concurrent.locks.Lock;

public class MainActivity extends AppCompatActivity {
    EditText txtAdivina;
    Button btnAdivina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAdivina = (EditText) findViewById(R.id.txtAdivina);
        btnAdivina = (Button) findViewById(R.id.btnAdivina);

        btnAdivina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ResultadoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("semana", txtAdivina.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
