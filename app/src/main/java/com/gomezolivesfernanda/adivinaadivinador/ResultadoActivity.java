package com.gomezolivesfernanda.adivinaadivinador;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

public class ResultadoActivity extends AppCompatActivity {
    TextView lblRsultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        lblRsultado = (TextView) findViewById(R.id.lblResultado);

        //bundle para recibir el parametro
        Bundle bundle = this.getIntent().getExtras();
        //Se guarda en una variable string el parametro recibido
        String resultado = bundle.getString("semana");
        //se convierte a entero el valor que el usuario puso
        int semanaUsuario = Integer.parseInt(resultado);
        //Se crea un objeto de la clase Calendar
        Calendar calendario = Calendar.getInstance();
        // Se guarda en una variable String la semana del año
        int semanaReal = calendario.get(Calendar.WEEK_OF_YEAR);

        //Se convierte a String la semana real para saber en el logcat que semana estamos
        String semanaString = Integer.toString(semanaReal);
        Log.e("semana", semanaString);

        //Se hace un if y else para preguntar si el usuario ha acertado la semana
        if (semanaUsuario == semanaReal){
            lblRsultado.setText("HAS ACERTADO");
            //Cambia el textView de color
            lblRsultado.setTextColor(getResources().getColor(R.color.Acertaste));
        }else {
            lblRsultado.setText("VUELVE A INTENTARLO");
            lblRsultado.setTextColor(getResources().getColor(R.color.Erraste));
        }
    }
}
